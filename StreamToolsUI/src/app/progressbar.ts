export class Progressbar {

  constructor(public id: number,
              public name: string,
              public currency: string,
              public currentValue: number,
              public goalValue: number,
              public color: string,
              public textColor: string) {
  }
}
