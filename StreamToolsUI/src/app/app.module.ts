import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {FormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {AppRoutingModule} from "./app.routing.module";
import {ProgressbarDashboardComponent} from './progressbar/dashboard/progressbar-dashboard.component';
import {ErrorInterceptor} from "./error.interceptor";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TokenInterceptor} from "./token.interceptor";
import {AuthService} from "./auth.service";
import {AuthGuard} from "./auth.guard";
import {ProgressbarFormComponent} from './progressbar/form/progressbar-form.component';
import {ProgressbarShowComponent} from './progressbar/show/progressbar-show.component';
import {ColorPickerModule} from "ngx-color-picker";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProgressbarDashboardComponent,
    ProgressbarFormComponent,
    ProgressbarShowComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ColorPickerModule,
    NgbModule.forRoot()
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    AuthService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
