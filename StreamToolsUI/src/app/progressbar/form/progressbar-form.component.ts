import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Progressbar} from '../../progressbar';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-progressbar-form',
  templateUrl: './progressbar-form.component.html',
  styleUrls: ['./progressbar-form.component.scss'],
  providers: [NgbActiveModal]
})
export class ProgressbarFormComponent implements OnInit {
  @Output() onSave: EventEmitter<Progressbar> = new EventEmitter();
  @Output() onCancel: EventEmitter<Progressbar> = new EventEmitter();
  @Input() model: Progressbar = new Progressbar(null, '', '', 0, 0, 'ffff00ff', '000000ff');
  @Input() buttonName = 'Create';
  @Input() buttonClass = 'btn btn-outline-primary';
  private startModel = {};
  private address = environment.apiUrl + '/progressbar/';

  constructor(private modalService: NgbModal, private http: HttpClient) {
  }

  open(content) {
    Object.assign(this.startModel, this.model);
    this.modalService.open(content).result.then(() => {
      this.save();
    }, () => {
      this.resetObject();
    });
  }

  ngOnInit() {
  }

  private resetObject() {
    Object.assign(this.model, this.startModel);
  }

  private save() {
    let saved;
    if (this.model.id) {
      saved = this.http.put(this.address + this.model.id, this.model);
    } else {
      saved = this.http.post(this.address, this.model);
      this.model = new Progressbar(null, '', '', 0, 0, 'ffff00ff', '000000ff');
    }
    saved.subscribe(
      res => this.afterSave(res)
    );
  }

  private afterSave(res) {
    this.onSave.emit(res);
  }
}
