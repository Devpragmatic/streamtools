import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProgressbarFormComponent} from './progressbar-form.component';

describe('ProgressbarFormComponent', () => {
  let component: ProgressbarFormComponent;
  let fixture: ComponentFixture<ProgressbarFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProgressbarFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressbarFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
