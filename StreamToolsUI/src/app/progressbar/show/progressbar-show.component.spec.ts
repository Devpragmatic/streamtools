import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressbarShowComponent } from './progressbar-show.component';

describe('ProgressbarShowComponent', () => {
  let component: ProgressbarShowComponent;
  let fixture: ComponentFixture<ProgressbarShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressbarShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressbarShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
