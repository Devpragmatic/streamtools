import {Component, OnInit} from '@angular/core';
import {NgbProgressbarConfig} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {Progressbar} from "../../progressbar";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'progressbar-progressbar-show',
  templateUrl: './progressbar-show.component.html',
  styleUrls: ['./progressbar-show.component.scss'],
  providers: [NgbProgressbarConfig]
})
export class ProgressbarShowComponent implements OnInit {

  id: number;
  progressbar: Progressbar = new Progressbar(null, '', '', 0, 0, 'ffff00ff', '000000ff');
  progressbarWidth = 0;

  constructor(private route: ActivatedRoute, private http: HttpClient) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.http.get<Progressbar>(environment.apiUrl + '/progressbar/' + this.id).subscribe(
        res => {
          this.progressbar = res;
          this.progressbar.textColor = this.convertHex(this.progressbar.textColor);
          this.progressbar.color = this.convertHex(this.progressbar.color);
          this.progressbarWidth = res.currentValue * 100 / res.goalValue;
        });
    });
  }

  private convertHex(hex) {
    const r = parseInt(hex.substring(0, 2), 16);
    const g = parseInt(hex.substring(2, 4), 16);
    const b = parseInt(hex.substring(4, 6), 16);
    const a = parseInt(hex.substring(6, 8), 16);
    return 'rgba(' + r + ',' + g + ',' + b + ',' + a / 255 + ')';
  }
}
