import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Progressbar} from "../../progressbar";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-progressbar-dashboard',
  templateUrl: './progressbar-dashboard.component.html',
  styleUrls: ['./progressbar-dashboard.component.scss']
})
export class ProgressbarDashboardComponent implements OnInit {

  progressbars = [];

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.getProgressBars();
  }

  addNewProgressBar(progressbar) {
    this.progressbars.push(progressbar);
  }

  getProgressBars() {
    this.http.get<Progressbar[]>(environment.apiUrl + "/progressbar/").subscribe(
      res => {
        this.progressbars = res;
      })
  }
}
