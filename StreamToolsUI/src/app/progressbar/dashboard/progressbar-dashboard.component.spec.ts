import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProgressbarDashboardComponent} from './progressbar-dashboard.component';

describe('ProgressbarDashboardComponent', () => {
  let component: ProgressbarDashboardComponent;
  let fixture: ComponentFixture<ProgressbarDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProgressbarDashboardComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressbarDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
