import {Injectable} from '@angular/core';
import {tokenNotExpired} from "angular2-jwt";

@Injectable()
export class AuthService {
  tokenName = 'Authorization';

  public getToken(): string {
    return localStorage.getItem(this.tokenName);
  }

  public setToken(token) {
    localStorage.setItem(this.tokenName, token);
  }

  public isAuthenticated(): boolean {
    var notExpired = tokenNotExpired(this.tokenName);
    if (!notExpired) {
      localStorage.removeItem(this.tokenName);
    }
    return notExpired
  }
}
