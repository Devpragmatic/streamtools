import {Component, OnInit} from '@angular/core';
import {environment} from '../../environments/environment';
import {User} from "../user";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {AuthService} from "../auth.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  model = new User("", "");

  constructor(private http: HttpClient, private router: Router, private authService: AuthService) {
  }

  ngOnInit() {
  }

  signUp() {
    this.http.post(environment.secureUrl + "/sign-up", this.model).subscribe(
      res => {
      },
      err => {
      }
    );
  }

  login() {
    this.http.post(environment.secureUrl + "/login", this.model, {observe: 'response'}).subscribe(
      res => {
        this.authService.setToken(res.headers.get(this.authService.tokenName));
        this.router.navigate([""]);
      },
      err => {
      }
    );
  }
}
