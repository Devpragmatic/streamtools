import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {LoginComponent} from './login/login.component';
import {ProgressbarDashboardComponent} from "./progressbar/dashboard/progressbar-dashboard.component";
import {AuthGuard} from "./auth.guard";
import {ProgressbarShowComponent} from "./progressbar/show/progressbar-show.component";

const routes: Routes = [
  {path: '', redirectTo: '/progressbar/dashboard', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'progressbar/show/:id', component: ProgressbarShowComponent},
  {path: 'progressbar/dashboard', component: ProgressbarDashboardComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
