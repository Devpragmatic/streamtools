package pl.improvementgroup.streamtools.progressbar.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pl.improvementgroup.streamtools.core.BaseIT;
import pl.improvementgroup.streamtools.progressbar.entity.ProgressBar;
import pl.improvementgroup.streamtools.progressbar.repository.ProgressBarRepository;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.improvementgroup.streamtools.progressbar.controller.ProgressBarController.API_PROGRESSBAR_URL;

public class ProgressBarControllerIT extends BaseIT {

    private static final String TEST_USER = "TestUser";
    private static final String WRONG_USER = "WrongUser";
    private static final String COLOR = "000000";
    private static final String TEXT_COLOR = "FFFFFF";
    private final ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    private ProgressBarRepository progressBarRepository;
    @Autowired
    private WebApplicationContext context;
    private MockMvc mvc;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

    }

    @After
    public void cleanUp() {
        progressBarRepository.deleteAll();
    }

    @Test
    @WithMockUser(username = TEST_USER)
    public void getShouldReturnUsersProgressBars() throws Exception {
        // given
        List<ProgressBar> progressBars = Arrays.asList(
                prepareProgressBar(TEST_USER),
                prepareProgressBar(WRONG_USER),
                prepareProgressBar(TEST_USER));

        progressBarRepository.save(progressBars);
        // when
        performGetAll()
                // then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    @WithMockUser(username = TEST_USER)
    public void getShouldReturnAllDataForUser() throws Exception {
        // given
        ProgressBar progressBar = prepareProgressBar(TEST_USER);
        progressBarRepository.save(progressBar);
        // when
        performGetAll()
                // then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", notNullValue()))
                .andExpect(jsonPath("$[0].color", is(progressBar.getColor())))
                .andExpect(jsonPath("$[0].ownerName", is(progressBar.getOwnerName())))
                .andExpect(jsonPath("$[0].currency", is(progressBar.getCurrency())))
                .andExpect(jsonPath("$[0].goalValue", is(progressBar.getGoalValue())))
                .andExpect(jsonPath("$[0].currentValue", is(progressBar.getCurrentValue())))
                .andExpect(jsonPath("$[0].name", is(progressBar.getName())));
    }

    @Test
    @WithMockUser(username = TEST_USER)
    public void postShouldCreateProgressBarWithOwner() throws Exception {
        // given
        ProgressBar progressBar = prepareProgressBar(TEST_USER);
        // when
        mvc.perform(
                post(API_PROGRESSBAR_URL)
                        .content(objectMapper.writeValueAsString(progressBar))
                        .contentType(MediaType.APPLICATION_JSON))
                // then
                .andExpect(status().isOk());
        List<ProgressBar> progressBars = progressBarRepository.findByOwnerName(TEST_USER);
        assertTrue(progressBars.size() == 1);
        ProgressBar createdProgressBar = progressBars.get(0);
        assertEquals(progressBar.getColor(), createdProgressBar.getColor());
        assertEquals(progressBar.getCurrency(), createdProgressBar.getCurrency());
        assertEquals(progressBar.getName(), createdProgressBar.getName());
        assertEquals(progressBar.getCurrentValue(), createdProgressBar.getCurrentValue());
        assertEquals(progressBar.getGoalValue(), createdProgressBar.getGoalValue());
        assertEquals(TEST_USER, createdProgressBar.getOwnerName());
    }

    @Test
    @WithMockUser(username = TEST_USER)
    public void getOneShouldReturnProgressBar() throws Exception {
        // given
        ProgressBar progressBar = prepareProgressBar(TEST_USER);
        progressBarRepository.save(progressBar);
        // when
        mvc.perform(
                get(API_PROGRESSBAR_URL + progressBar.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                // then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", notNullValue()))
                .andExpect(jsonPath("$.color", is(progressBar.getColor())))
                .andExpect(jsonPath("$.textColor", is(progressBar.getTextColor())))
                .andExpect(jsonPath("$.ownerName", is(progressBar.getOwnerName())))
                .andExpect(jsonPath("$.currency", is(progressBar.getCurrency())))
                .andExpect(jsonPath("$.goalValue", is(progressBar.getGoalValue())))
                .andExpect(jsonPath("$.currentValue", is(progressBar.getCurrentValue())))
                .andExpect(jsonPath("$.name", is(progressBar.getName())));
    }

    private ProgressBar prepareProgressBar(String ownerName) {
        ProgressBar progressBar = prepareProgressBar();
        progressBar.setOwnerName(ownerName);
        return progressBar;
    }

    private ProgressBar prepareProgressBar() {
        ProgressBar progressBar = new ProgressBar();
        progressBar.setColor(COLOR);
        progressBar.setTextColor(TEXT_COLOR);
        progressBar.setCurrency(getRandomString());
        progressBar.setGoalValue(getRandomLong());
        progressBar.setCurrentValue(getRandomLong());
        progressBar.setName(getRandomString());
        return progressBar;
    }

    private ResultActions performGetAll() throws Exception {
        return mvc.perform(
                get(API_PROGRESSBAR_URL)
                        .contentType(MediaType.APPLICATION_JSON));
    }
}