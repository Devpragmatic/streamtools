package pl.improvementgroup.streamtools.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pl.improvementgroup.streamtools.core.BaseIT;
import pl.improvementgroup.streamtools.security.entity.ApplicationUser;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class SecuritySecurityIT extends BaseIT {
    private static final String PASSWORD = "password";
    private static ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void shouldNotLogInWithoutSignUp() throws Exception {
        String user = mapper.writeValueAsString(new ApplicationUser("username", PASSWORD));
        // when
        doLogin(user)
                // then
                .andExpect(status().isUnauthorized())
                .andExpect(header().string(SecurityConstant.AUTHORIZATION, nullValue()));
    }

    @Test
    public void shouldSingUp() throws Exception {
        String user = mapper.writeValueAsString(new ApplicationUser("username1", PASSWORD));
        // when
        doSingUp(user)
                .andExpect(status().isNoContent());

        // then
        doLogin(user)
                .andExpect(status().isOk())
                .andExpect(header().string(SecurityConstant.AUTHORIZATION, notNullValue()));
    }

    @Test
    public void shouldNotSingUpTwice() throws Exception {
        String user = mapper.writeValueAsString(new ApplicationUser("username2", PASSWORD));
        // when
        doSingUp(user)
                .andExpect(status().isNoContent());

        // then
        doSingUp(user)
                .andExpect(status().isConflict());
    }

    private ResultActions doLogin(String userJson) throws Exception {
        return mvc.perform(
                post(SecurityConstant.LOGIN_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(userJson));
    }

    private ResultActions doSingUp(String userJson) throws Exception {
        return mvc.perform(
                post(SecurityConstant.SIGN_UP_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(userJson));
    }

}