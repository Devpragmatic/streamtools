package pl.improvementgroup.streamtools.core;

import org.apache.commons.lang3.RandomUtils;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public abstract class BaseIT {

    protected static String getRandomString() {
        return randomAlphabetic(8, 32);
    }

    protected static Long getRandomLong() {
        return RandomUtils.nextLong();
    }
}
