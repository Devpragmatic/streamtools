package pl.improvementgroup.streamtools;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreamToolsApplication {

    public static void main(String[] args) {
        SpringApplication.run(StreamToolsApplication.class, args);
    }
}
