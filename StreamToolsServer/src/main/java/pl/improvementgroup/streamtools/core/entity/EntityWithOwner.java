package pl.improvementgroup.streamtools.core.entity;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class EntityWithOwner extends EntityBase{

    private String ownerName;

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }
}
