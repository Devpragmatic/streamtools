package pl.improvementgroup.streamtools.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import pl.improvementgroup.streamtools.core.entity.EntityWithOwner;

import java.util.List;

@NoRepositoryBean
public interface EntityPerUserRepository<T extends EntityWithOwner> extends JpaRepository<T, Long> {

    List<T> findByOwnerName(String ownerName);
}
