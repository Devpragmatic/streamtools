package pl.improvementgroup.streamtools.security.filter;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;
import pl.improvementgroup.streamtools.security.property.JwtProperties;
import pl.improvementgroup.streamtools.security.service.TokenAuthenticationService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


public class JWTAuthenticationFilter extends GenericFilterBean {

    private final JwtProperties jwtProperties;
    private final TokenAuthenticationService tokenAuthenticationService;

    public JWTAuthenticationFilter(JwtProperties jwtProperties, TokenAuthenticationService tokenAuthenticationService) {
        this.jwtProperties = jwtProperties;
        this.tokenAuthenticationService = tokenAuthenticationService;
    }

    @Override
    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain filterChain)
            throws IOException, ServletException {
        Authentication authentication = tokenAuthenticationService.getAuthentication((HttpServletRequest) request, jwtProperties);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        filterChain.doFilter(request, response);
    }
}