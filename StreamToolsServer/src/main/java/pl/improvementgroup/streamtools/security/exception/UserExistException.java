package pl.improvementgroup.streamtools.security.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class UserExistException extends RuntimeException {

    private static final String MESSAGE = "User with this login exists";

    public UserExistException() {
        super(MESSAGE);
    }
}
