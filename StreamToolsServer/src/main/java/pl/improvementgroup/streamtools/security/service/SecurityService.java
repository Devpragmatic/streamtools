package pl.improvementgroup.streamtools.security.service;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import pl.improvementgroup.streamtools.security.entity.ApplicationUser;
import pl.improvementgroup.streamtools.security.exception.UserExistException;
import pl.improvementgroup.streamtools.security.repository.ApplicationUserRepository;

import javax.validation.constraints.NotNull;

@Service
public class SecurityService {

    private ApplicationUserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    public SecurityService(ApplicationUserRepository userRepository,
                           PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void signUp(@RequestBody @NotNull ApplicationUser applicationUser) {
        if (userWithUsernameExists(applicationUser)) {
            throw new UserExistException();
        }
        applicationUser.setPassword(passwordEncoder.encode(applicationUser.getPassword()));
        userRepository.save(applicationUser);
    }

    private boolean userWithUsernameExists(@NotNull ApplicationUser applicationUser) {
        return userRepository.findByUsername(applicationUser.getUsername()) != null;
    }
}
