package pl.improvementgroup.streamtools.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.improvementgroup.streamtools.security.entity.ApplicationUser;

public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {
    ApplicationUser findByUsername(String username);
}
