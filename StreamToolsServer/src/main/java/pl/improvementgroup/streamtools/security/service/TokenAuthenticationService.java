package pl.improvementgroup.streamtools.security.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import pl.improvementgroup.streamtools.security.property.JwtProperties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

import static java.util.Collections.emptyList;
import static pl.improvementgroup.streamtools.security.SecurityConstant.AUTHORIZATION;

@Service
public class TokenAuthenticationService {

    public void addAuthentication(HttpServletResponse res, String username, JwtProperties jwtProperties) {
        String jwt = Jwts.builder()
                .setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + jwtProperties.getExpirationTimeInMiliseconds()))
                .signWith(SignatureAlgorithm.HS512, jwtProperties.getSecretKey())
                .compact();
        res.addHeader(AUTHORIZATION, jwtProperties.getTokenPrefix() + jwt);
    }

    public Authentication getAuthentication(HttpServletRequest request, JwtProperties jwtProperties) {
        String token = request.getHeader(AUTHORIZATION);
        if (!StringUtils.isEmpty(token)) {
            // parse the token.
            String user = Jwts.parser()
                    .setSigningKey(jwtProperties.getSecretKey())
                    .parseClaimsJws(token.replace(jwtProperties.getTokenPrefix(), ""))
                    .getBody()
                    .getSubject();
            return user != null ?
                    new UsernamePasswordAuthenticationToken(user, null, emptyList()) :
                    null;
        }
        return null;
    }
}