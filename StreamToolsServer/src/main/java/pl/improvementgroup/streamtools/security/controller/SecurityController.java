package pl.improvementgroup.streamtools.security.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.improvementgroup.streamtools.security.SecurityConstant;
import pl.improvementgroup.streamtools.security.entity.ApplicationUser;
import pl.improvementgroup.streamtools.security.service.SecurityService;

import javax.validation.constraints.NotNull;

@RestController
public class SecurityController {

    private SecurityService securityService;

    public SecurityController(SecurityService securityService) {
        this.securityService = securityService;
    }

    @PostMapping(SecurityConstant.SIGN_UP_URL)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void signUp(@RequestBody @NotNull ApplicationUser applicationUser) {
        securityService.signUp(applicationUser);
    }
}
