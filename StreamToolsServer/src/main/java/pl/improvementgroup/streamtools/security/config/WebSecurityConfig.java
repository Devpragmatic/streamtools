package pl.improvementgroup.streamtools.security.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import pl.improvementgroup.streamtools.security.filter.JWTAuthenticationFilter;
import pl.improvementgroup.streamtools.security.filter.JWTAuthorizationFilter;
import pl.improvementgroup.streamtools.security.property.JwtProperties;
import pl.improvementgroup.streamtools.security.service.TokenAuthenticationService;

import static pl.improvementgroup.streamtools.security.SecurityConstant.LOGIN_URL;

public abstract class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String[] FRONTEND_PATTERNS = new String[]{"/*.html", "/*.js", "/*.ico", "/assets/**", "/"};
    private final JwtProperties jwtProperties;
    private final UserDetailsService userDetailsService;
    private final TokenAuthenticationService tokenAuthenticationService;

    public WebSecurityConfig(JwtProperties jwtProperties, UserDetailsService userDetailsService, TokenAuthenticationService tokenAuthenticationService) {
        this.jwtProperties = jwtProperties;
        this.userDetailsService = userDetailsService;
        this.tokenAuthenticationService = tokenAuthenticationService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .addFilterBefore(new JWTAuthorizationFilter(LOGIN_URL, authenticationManager(), jwtProperties, tokenAuthenticationService),
                        UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(new JWTAuthenticationFilter(jwtProperties, tokenAuthenticationService), UsernamePasswordAuthenticationFilter.class);
        http.csrf().disable();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers(FRONTEND_PATTERNS);
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
