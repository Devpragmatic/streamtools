package pl.improvementgroup.streamtools.security;

public final class SecurityConstant {

    public static final String AUTHORIZATION = "Authorization";

    private static final String BASE_URL = "/api/secure";
    public static final String SIGN_UP_URL = BASE_URL + "/sign-up";
    public static final String LOGIN_URL = BASE_URL + "/login";

    private SecurityConstant() {
    }
}
