package pl.improvementgroup.streamtools.progressbar.entity;

import pl.improvementgroup.streamtools.core.entity.EntityWithOwner;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.awt.*;
import java.math.BigInteger;

@Entity
public class ProgressBar extends EntityWithOwner {

    @NotNull
    private String name;
    @NotNull
    private String currency;
    @NotNull
    private Long currentValue;
    @NotNull
    private Long goalValue;
    @NotNull
    private Color color = Color.RED;
    @NotNull
    private Color textColor = Color.RED;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Long getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(Long currentValue) {
        this.currentValue = currentValue;
    }

    public Long getGoalValue() {
        return goalValue;
    }

    public void setGoalValue(Long goalValue) {
        this.goalValue = goalValue;
    }

    public String getColor() {
        return Integer.toHexString(Integer.rotateLeft(this.color.getRGB(), 8));
    }

    public void setColor(String color) {
        this.color = new Color(Integer.rotateRight(new BigInteger(color, 16).intValue(), 8));
    }

    public String getTextColor() {
        return Integer.toHexString(Integer.rotateLeft(this.textColor.getRGB(), 8));
    }

    public void setTextColor(String textColor) {
        this.textColor = new Color(Integer.rotateRight(new BigInteger(textColor, 16).intValue(), 8));
    }
}
