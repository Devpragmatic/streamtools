package pl.improvementgroup.streamtools.progressbar.exception;

public class CreatedEntityHasIdException extends RuntimeException {

    private static final String MESSAGE = "Created entity shouldn't has id.";

    public CreatedEntityHasIdException() {
        super(MESSAGE);
    }
}
