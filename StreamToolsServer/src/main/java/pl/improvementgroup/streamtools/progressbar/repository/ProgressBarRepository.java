package pl.improvementgroup.streamtools.progressbar.repository;

import org.springframework.stereotype.Repository;
import pl.improvementgroup.streamtools.core.repository.EntityPerUserRepository;
import pl.improvementgroup.streamtools.progressbar.entity.ProgressBar;

@Repository
public interface ProgressBarRepository extends EntityPerUserRepository<ProgressBar> {
}
