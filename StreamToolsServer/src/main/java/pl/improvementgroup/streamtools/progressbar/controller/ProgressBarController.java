package pl.improvementgroup.streamtools.progressbar.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.improvementgroup.streamtools.progressbar.entity.ProgressBar;
import pl.improvementgroup.streamtools.progressbar.exception.CreatedEntityHasIdException;
import pl.improvementgroup.streamtools.progressbar.repository.ProgressBarRepository;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping(value = ProgressBarController.API_PROGRESSBAR_URL)
public class ProgressBarController {

    public static final String API_PROGRESSBAR_URL = "/api/progressbar/";

    private final ProgressBarRepository progressBarRepository;

    public ProgressBarController(ProgressBarRepository progressBarRepository) {
        this.progressBarRepository = progressBarRepository;
    }

    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    @ResponseBody
    public List<ProgressBar> get(Principal principal) {
        return progressBarRepository.findByOwnerName(principal.getName());
    }

    @RequestMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    @ResponseBody
    public ProgressBar getOne(@PathVariable Long id) {
        return progressBarRepository.findOne(id);
    }

    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    @ResponseBody
    public ProgressBar create(@RequestBody ProgressBar progressBar, Principal principal) {
        if (progressBar.getId() != null) {
            throw new CreatedEntityHasIdException();
        }
        progressBar.setOwnerName(principal.getName());
        return progressBarRepository.save(progressBar);
    }

    @RequestMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT)
    @ResponseBody
    public ProgressBar update(@PathVariable Long id, @RequestBody ProgressBar progressBar, Principal principal) {
        verifyAccess(principal, id);
        progressBar.setId(id);
        progressBar.setOwnerName(principal.getName());
        return progressBarRepository.save(progressBar);
    }

    @RequestMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id, Principal principal) {
        verifyAccess(principal, id);
        progressBarRepository.delete(id);
    }

    private void verifyAccess(Principal principal, Long id) {
        ProgressBar existingProgressBar = progressBarRepository.findOne(id);
        if (existingProgressBar == null) {
            throw new IllegalArgumentException();
        }
        verifyAccess(principal, existingProgressBar);
    }

    private void verifyAccess(Principal principal, ProgressBar progressBar) {
        if (!progressBar.getOwnerName().equals(principal.getName())) {
            throw new IllegalArgumentException();
        }
    }
}
