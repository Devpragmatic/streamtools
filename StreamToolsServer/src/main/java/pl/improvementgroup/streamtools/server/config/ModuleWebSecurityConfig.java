package pl.improvementgroup.streamtools.server.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import pl.improvementgroup.streamtools.progressbar.controller.ProgressBarController;
import pl.improvementgroup.streamtools.security.config.WebSecurityConfig;
import pl.improvementgroup.streamtools.security.property.JwtProperties;
import pl.improvementgroup.streamtools.security.service.TokenAuthenticationService;

import static pl.improvementgroup.streamtools.security.SecurityConstant.LOGIN_URL;
import static pl.improvementgroup.streamtools.security.SecurityConstant.SIGN_UP_URL;

@Configuration
@EnableWebSecurity
public class ModuleWebSecurityConfig extends WebSecurityConfig {

    @Autowired
    public ModuleWebSecurityConfig(JwtProperties jwtProperties, UserDetailsService userDetailsService, TokenAuthenticationService tokenAuthenticationService) {
        super(jwtProperties, userDetailsService, tokenAuthenticationService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, ProgressBarController.API_PROGRESSBAR_URL + "*").permitAll()
                .antMatchers(SIGN_UP_URL).permitAll()
                .antMatchers(LOGIN_URL).permitAll()
                .anyRequest().authenticated();
        super.configure(http);
    }
}
